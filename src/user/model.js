const db = require('./../configs/sequelize');
const {DataTypes, Model } = db.Sequelize;

const sequelize = db.sequelize;

class User extends Model {}

User.init({
  // Model attributes are defined here
  nome: {
    type: DataTypes.STRING
  },
  email: {
    type: DataTypes.STRING
    // allowNull defaults to true
  }
}, {
  // Other model options go here
  sequelize, // We need to pass the connection instance
  modelName: 'users' // We need to choose the model name
});

module.exports = User;