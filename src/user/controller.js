const db = require('./../configs/sequelize');
const User = require('./model');


exports.create = (req,res) =>{
    User.create({
        nome: req.body.nome,
        email: req.body.email
    }).then((user) => {
        res.send(user);
    });
}


exports.update = (req, res) => {
    try {
        User.update({
            nome: req.body.nome,
            email: req.body.email
        }, {
            where: {
                id: req.body.id
            }
        }).then((user) => {
            res.send(user);
        }).catch((err) => {
            res.send(err);
        });
    } catch (error) {
        console.log(error);
    }

}

exports.destroy = (req, res) => {
    try {
        User.destroy({
            where: {
                id: req.body.id
            }
        }).then((user) => {
            res.send(true);
        }).catch((err) => {
            res.send(err);
        });
    } catch (error) {
        console.log(error);
    }

}

exports.findAll = (req,res) =>{
    User.findAll().then((users) => {
        res.send(users);
    });
}