module.exports = (app) => {

    const controller = require('./controller');

    app.post('/usuarios', controller.create);

    app.get('/usuarios', controller.findAll);

    app.put('/usuarios', controller.update);
    
    app.delete('/usuarios', controller.destroy);
} 