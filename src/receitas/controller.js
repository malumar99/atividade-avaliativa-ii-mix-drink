const db = require('../configs/sequelize');
const Receita = require('./model');
const ItensReceita = require('./../itens_receita/model');

exports.create = (req,res) =>{
    try {
        Receita.create({
            nome: req.body.nome,
            descricao: req.body.descricao,
            tipo_id: req.body.tipo_id,
            tempo: req.body.tempo,
            preparo: req.body.preparo,
            rendimento: req.body.rendimento,
            status: 1,
            aprovacao: 1,
            user_id: null

        }).then((receita) => {
            res.send(receita);
        }).catch((err)=> {
            //console.log('aaaaaaaaaaa2:',err);
        });

    } catch (error) {
        //console.log('aaaaaaaaaaa:',error);
    }
}


// exports.findAll = (req,res) =>{
//     Receita.findAll().then((receitas) => {
//         res.send(receitas);
//     });
// }


exports.findAll = (req,res) =>{
    //console.log('>>>>', req.params.id);

    db.sequelize.query(`

        Select receitas.*, "tiposReceitas".descricao from receitas 
        Join "tiposReceitas" on "tiposReceitas".id = "receitas".tipo_id;

    `).then(async (receita) => {
        res.send(receita[0]);
    }).catch((err) => {
        res.send(err);
    });

}


exports.update = (req, res) => {
    try {
        Receita.update({
            nome: req.body.nome,
            descricao: req.body.descricao,
            tipo_id: req.body.tipo_id,
            tempo: req.body.tempo,
            preparo: req.body.preparo,
            rendimento: req.body.rendimento
        }, {
            where: {
                id: req.body.id
            }
        }).then((user) => {
            res.send(user);
        }).catch((err) => {
            res.send(err);
        });
    } catch (error) {
        //console.log(error);
    }

}


exports.visualizar = (req, res) => {
    try {

        console.log('>>>>', req.params.id);

        var where = "";
        if(req.params.id != undefined && req.params.id != null && req.params.id != '' && req.params.id != 'undefined' ){
            where = " where tipo_id = "+req.params.id;
        }

        db.sequelize.query(`
            Select receitas.*, "tiposReceitas".descricao from receitas 
            Join "tiposReceitas" on receitas.tipo_id = "tiposReceitas".id
            ${where};
        `).then(async (receita) => {

            for (let i = 0; i < receita[0].length; i++) {
                //const element = receita[0][i];

                //console.log("aaaaa::::", receita[0][i].id);

                var a = await db.sequelize.query(`
                
                Select * from "itensReceitas" 
                Join medidas on medidas.id = "itensReceitas".medida_id
                Join ingredientes on ingredientes.id = "itensReceitas".ingrediente_id
                where receita_id = ${receita[0][i].id};

                `).then((itens) => {
                    return itens[0];
                }).catch((err) => {
                    //console.log(error);
                });

                //console.log("aaaaa", a);

                receita[0][i].itens = a;
                
            }

            res.send(receita[0]);
        }).catch((err) => {
            res.send(err);
        });
    } catch (error) {
        //console.log(error);
    }

}


exports.destroy = (req, res) => {
    try {

        var deleteItens = ItensReceita.destroy({
            where: {
                receita_id: req.body.id
            }
        }).then((user) => {
            return true;//res.send(true);
        }).catch((err) => {
            res.send(err);
        });


        Receita.destroy({
            where: {
                id: req.body.id
            }
        }).then((user) => {
            res.send(true);
        }).catch((err) => {
            res.send(err);
        });
    } catch (error) {
        //console.log(error);
    }

}

exports.insert = (req, res) => {
    try {
        db.sequelize.query(`
        insert INTO receitas (id, nome, descricao, tipo_id, tempo, preparo, rendimento, status, "createdAt", "updatedAt", aprovacao) VALUES
	(1, 'Drink dos Deuses', 'show de drink', 2, 5, 'Bater os 5 primeiros ingredientes no liquidificador e acrescentar o gelo na hora de servir', 9, 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00', 1),
	(2, 'Vinho Quente', '', 2, 20, '1- Queime a metade do açúcar com o cravo e a canela.\r\n2- Acrescente o vinho, já misturado com a água.\r\n3- Junte a maçã descascada e picada e o açúcar restante.\r\n4- Deixe cozinhar um pouco e sirva bem quente.\r\n', 10, 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00', 1),
	(3, 'Bebida tipo Amarula', '', 2, 5, '1- Bater tudo no liquidificador, com gelo a gosto.\r\n2- Servir gelado.', 10, 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00', 1),
	(4, 'Quentão', '', 2, 40, '1- Colocar em uma panela grande o açúcar, as cascas de laranja, de limão, gengibre em pedaços, cravo e a canela.\r\n2- Quando o açúcar estiver derretendo colocar a cachaça e a água, deixando cozinhar por 20 a 25 minutos em fogo médio.\r\n3- Filtre, e após coloque a maçã picada\r\n4- Manter no fogo, após o preparo.\r\n', 8, 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00', 1),
	(5, 'Batida de Sonho de Valsa', '', 2, 10, '1- Bater tudo no liquidificador, ficando os chocolates crocantes por cima. Servir gelado.\r\n', 5, 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00', 1),
	(6, 'Mojito tradicional cubano', '', 2, 15, '1- Coloque no copo onde vai ser servido o drink, os 4 últimos ingredientes.\r\n2- Amasse bem o hortelã.\r\n3- Depois é só adicionar o Bacardi e o gelo.\r\n', 5, 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00', 1),
	(7, 'Batidinha de frutas', '', 2, 10, '1 - Basta bater todos os ingredientes no liquidificador e servir\r\n2 - Taças decoradas com flores e frutas dão um toque special\r\n', 4, 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00', 1),
	(8, 'Nevada', '', 2, 5, '1 - Em um liquidificador coloque o gelo picado, esprema o limão (não colocar o limão), acrescente o leite condensado, cerca de 50 ml, e vodka a gosto\r\n2 - Bata tudo até que a consistência lembre clara em neve\r\n3 - Se precisar, acrescente mais gelo ou leite condensado\r\n4 - Coloque em uma taça e enfeite com meia banda de uma rodela de limão\r\n', 1, 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00', 1),
	(9, 'Afrodite', '', 2, 5, '1 - Bata tudo no liquidificador e sirva a seguir', 4, 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00', 1),
	(10, 'Dry martini', '', 2, 5, '1 - Gele uma taça de coquetel e reserve\r\n2 - Em uma coqueteleira com gelo, coloque o vermouth e o gin (não é necessário agitar)\r\n3 - Mexa com uma colher de cabo longo, utilizando o passador\r\n4  Coe o coquetel para a taça e torça a bebida uma fatia fina da casca de um limão (twist) e sirva\r\n5 - Pode-se decorar com 1 azeitona verde espetada em um palito\r\n', 0, 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00', 1),
	(11, 'Falso champagne', '', 2, 2, '1 - Misture os dois ingredientes e sirva', 2, 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00', 1),
	(12, 'Ice Caseira', '', 2, 3, '1 - Misture tudo!\r\n', 5, 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00', 1),
	(13, 'Beijo na boca', '', 2, 10, '1 - Misture todos os ingredientes\r\n2 - Bata no liquidificador\r\n3 - Adicione gelo a vontade\r\n', 3, 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00', 1),
	(14, 'Fresh', '', 2, 7, '1 - Misture os ingredientes numa coqueteleira e mexa bem por 5 minutos e sirva em copo longo', 2, 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00', 1),
	(15, 'Vinho delicia', '', 2, 10, '1 - Bata todos os ingredientes no liquidificador e sirva gelado', 30, 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00', 1),
	(16, 'Pina Colada', '', 2, 2, 'Coloque os ingredientes em ordem em uma coqueteleira\r\nBata por aproximadamente 10 segundos\r\nEnfeite um copo com calda de chocolate e um pedaço pequeno de abacaxi\r\nPor cima coloque coco ralado\r\n', 1, 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00', 1),
	(17, 'Quentão do Sul', '', 2, 20, 'Faça um chá com os ingredientes exceto o vinho, para que soltem os seus aromas\r\nPode ferver por 15 minutos\r\nAcrescente o vinho e sirva quando estiver bem quente, com canela polvilhada na própria xícara\r\nObs1:\r\nQuanto menor o pedaço de gengibre (exemplo em rodelas) mais forte fica\r\nObs2:\r\nCuidado para não ferver muito, senão o álcool evapora e o sabor muda', 10, 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00', 1),
	(18, 'Coquetel de Frutas', '', 2, 10, 'Bater tudo no liquidificador\r\nServir bem gelado\r\n', 6, 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00', 1),
	(19, 'Bebida do Abacaxi', '', 2, 5, 'Retire toda a parte de dentro do abacaxi deixando só a casca\r\nColoque a vodka\r\nAdicione o leite condensado, misture e sirva\r\n', 1, 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00', 1),
	(20, 'Sangria Espanhola', '', 2, 15, 'Ponha as frutas numa jarra grande de vidro ou uma tigela, com o açúcar\r\nAdicione o suco de laranja, o vinho, os cravos, o refrigerante, o licor e o gelo\r\nEnfeite a jarra com cascas de laranja e de limão\r\n', 15, 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00', 1),
	(21, 'Licor de Jabuticaba', '', 2, 10, 'Lave as jabuticabas e deixe escorrer (reserve)\r\nDê um leve corte em cada jabuticaba, não a separe em duas partes, apenas até o meio, para que o sabor de dentro possa passar para o alcoól \r\nColoque em um recipiente de plástico ou vidro com tampa, o açúcar, a pinga e a jabuticaba, mexa e deixe curtir por 15 dias, mexendo uma vez por dia\r\nFinalizando os 15 dias coar e engarrafar\r\n', 2, 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00', 1),
	(22, 'Sonho dos Anjinhos', '', 2, 5, 'Bata todos os ingredientes do liquidificador e sirva.', 5, 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00', 1),
	(23, 'Licor de Chocolate', '', 2, 10, 'Colocar no liquidificador primeiramente os 3 primeiros ingredientes, bater\r\nAcrescentar os outros ingredientes\r\nBater bem\r\nDeixar descansar durante 5 dias\r\nManter fora da geladeira\r\n', 2, 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00', 1),
	(24, 'Batida de Coco', '', 2, 30, 'Coloque no liquidificador o leite de coco, o leite condensado, o coco ralado, o açúcar e a água e bata até misturar bem\r\nSepare numa vasilha metade da mistura\r\nAdicione metade da pinga e bata novamente (coloque numa garrafa de 2 litros)\r\nRepita o mesmo com a outra metade\r\nProve e saboreie se está bom de açúcar e pinga\r\n', 6, 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00', 1),
	(25, 'Drink Divorce', '', 2, 5, 'Bata tudo no liquidificador, e se preferir acrescente gelo picado', 10, 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00', 1),
	(26, 'Batida de Maracujá', '', 2, 5, 'Bata todos os ingredientes no liquidificador\r\nSirva gelado\r\n', 4, 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00', 1),
	(27, 'Caipirinha de Vodka', '', 2, 5, 'Corte o limão ao meio e depois cada uma das partes novamente ao meio\r\nColoque na coqueteleira e adicione todo o açúcar\r\nAmasse o limão juntamente com o açúcar\r\nAcrescente a vodka e o gelo\r\nAgite muito bem e pronto\r\n', 1, 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00', 1),
	(28, 'Ponche Delicado', '', 2, 5, 'Misture as bebidas com as frutas e o gelo picado, diretamente na poncheira\r\nSirva em copos de ponche', 10, 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00', 1),
	(29, 'Margarita', '', 2, 3, 'Molhe a boca do copo para coquetel com suco de limão e encoste no sal para formar uma borda\r\nJunte em uma coqueteleira a Tequila, o Contreau, o suco de limão, e 1 xícara de cubos de gelo\r\nMisture bem e coe sobre\r\n', 1, 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00', 1),
	(30, 'Drink Tropical', '', 2, 10, 'Com o auxílio de um cortador redondo de biscoitos ou uma faca, faça uma pequena abertura na casca da melancia\r\nAjeite um batedor da batedeira pela abertura da melancia e bata, para que o suco da fruta vá se soltando\r\nDespeje o suco no liquidificador\r\nEsprema o suco coado de 2 laranjas, 2 limões-sicilianos, 2 tangerinas e 2 limões taiti e adicione ao liquidificador\r\nAdicione 300 ml de vodka e bata mais um pouco\r\nCorte o topo da melancia, fazendo da casca da fruta uma tigela e despeje a bebida dentro\r\nAdicione açúcar e gelo a gosto\r\n', 6, 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00', 1),
	(31, 'Batidinha de Bis', '', 2, 10, 'Misture todos os ingredientes no liquidificador\r\nFica de sua preferência a bebida mais forte ou suave (forte acrescente mais vodka, suave acrescente leite ou água)\r\nSirva gelado', 5, 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00', 1),
	(32, 'Batida Frozen Skol Beats', '', 2, 5, 'Esprema os dois limões, extraindo o seu suco\r\nBata todos ingredientes no liquidificador e adicione pedras de gelo se desejar', 4, 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00', 1),
	(33, 'Batida de Pêssego', '', 2, 15, 'Bater todos os ingredientes no liquidificador, até que ela fique cremosa e é só saborear', 50, 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00', 1),
	(34, 'Leite de Onça', '', 2, 5, 'Bater tudo no liquidificador\r\n', 4, 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00', 1),
	(35, 'Mamadeira de Anjo', '', 2, 10, 'Bata todos os ingredientes no liquidificador\r\nSirva com gelo', 8, 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00', 1),
	(36, 'Beijo Danyseven', '', 2, 10, 'Misturar bem na coqueteleira e servir com gelo', 1, 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00', 0)`).then((receitas) => {
            res.send(receitas);
        }).catch((err) => {
            res.send(err);
        });
    } catch (error) {
        //console.log(error);
    }

}