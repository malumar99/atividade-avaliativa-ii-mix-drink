module.exports = (app) => {

    const controller = require('./controller');

    app.post('/receitas', controller.create);

    app.get('/receitas', controller.findAll);

    app.put('/receitas', controller.update);

    app.delete('/receitas', controller.destroy);

    app.post('/insert-receitas', controller.insert);

    app.get('/receitas-visualizar/:id', controller.visualizar);
} 