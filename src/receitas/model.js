const db = require('../configs/sequelize');
const {DataTypes, Model } = db.Sequelize;

const sequelize = db.sequelize;

const TiposReceita = require('./../tipos_receita/model');
const User = require('./../user/model');

class Receita extends Model {}



Receita.init({
  nome: {
    type: DataTypes.STRING
  },
  descricao: {
    type: DataTypes.STRING
  },
  tipo_id: {
    type: DataTypes.INTEGER
  },
  tempo: {
    type: DataTypes.INTEGER
  },
  preparo: {
    type: DataTypes.STRING,
    length: 4000
  },
  rendimento: {
    type: DataTypes.INTEGER
  },
  status: {
    type: DataTypes.INTEGER
  },
  aprovacao: {
    type: DataTypes.INTEGER
  },
  user_id: {
    type: DataTypes.INTEGER
  },
  
}, {
  sequelize,
  modelName: 'receitas'
});

Receita.belongsTo(TiposReceita, { foreignKey: 'tipo_id' });
Receita.belongsTo(User, { foreignKey: 'user_id' });

module.exports = Receita;