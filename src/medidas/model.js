const db = require('../configs/sequelize');
const {DataTypes, Model } = db.Sequelize;

const sequelize = db.sequelize;

class Medida extends Model {}


Medida.init({
  descricao: {
    type: DataTypes.STRING
  },
  status: {
    type: DataTypes.INTEGER
  }
}, {
  sequelize,
  modelName: 'medidas'
});

module.exports = Medida;