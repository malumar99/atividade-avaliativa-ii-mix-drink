const db = require('../configs/sequelize');
const Medida = require('./model');


exports.create = (req,res) =>{
    Medida.create({
        descricao: req.body.descricao,
        status: req.body.status
    }).then((medida) => {
        res.send(medida);
    });
}


exports.findAll = (req,res) =>{
    Medida.findAll().then((medidas) => {
        res.send(medidas);
    });
}

exports.update = (req, res) => {
    try {
        Medida.update({
            descricao: req.body.descricao,
            status: req.body.status
        }, {
            where: {
                id: req.body.id
            }
        }).then((user) => {
            res.send(user);
        }).catch((err) => {
            res.send(err);
        });
    } catch (error) {
        console.log(error);
    }

}

exports.destroy = (req, res) => {
    try {
        Medida.destroy({
            where: {
                id: req.body.id
            }
        }).then((user) => {
            res.send(true);
        }).catch((err) => {
            res.send(err);
        });
    } catch (error) {
        console.log(error);
    }

}

exports.insert = (req, res) => {
    try {
        db.sequelize.query(`
        insert INTO medidas (id, descricao, status, "createdAt", "updatedAt") VALUES
        (4, 'a gosto', 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00'),
        (12, 'bombons', 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00'),
        (24, 'cacho', 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00'),
        (5, 'casca', 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00'),
        (11, 'colher', 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00'),
        (23, 'colher (café)', 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00'),
        (10, 'colheres (sopa)', 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00'),
        (8, 'copo', 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00'),
        (17, 'copo americano', 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00'),
        (14, 'dose', 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00'),
        (6, 'folha', 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00'),
        (13, 'folhas', 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00'),
        (1, 'g', 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00'),
        (19, 'garrafa', 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00'),
        (25, 'kg', 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00'),
        (3, 'lata', 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00'),
        (9, 'litro', 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00'),
        (2, 'ml', 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00'),
        (22, 'pedaços', 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00'),
        (20, 'Pedras', 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00'),
        (26, 'pote', 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00'),
        (21, 'rama', 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00'),
        (27, 'tablete', 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00'),
        (7, 'unidade', 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00'),
        (15, 'xícara', 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00'),
        (16, 'xícaras', 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00')`).then((medidas) => {
            res.send(medidas);
        }).catch((err) => {
            res.send(err);
        });
    } catch (error) {
        console.log(error);
    }

}