module.exports = (app) => {

    const controller = require('./controller');

    app.post('/medidas', controller.create);

    app.get('/medidas', controller.findAll);

    app.put('/medidas', controller.update);

    app.delete('/medidas', controller.destroy);

    app.post('/insert-medidas', controller.insert);
} 