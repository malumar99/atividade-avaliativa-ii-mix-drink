const db = require('../configs/sequelize');
const {DataTypes, Model } = db.Sequelize;

const sequelize = db.sequelize;

const Medida = require('./../medidas/model');
const Ingrediente = require('./../ingredientes/model');
const Receita = require('./../receitas/model');

class ItensReceita extends Model {}


ItensReceita.init({
  receita_id: {
    type: DataTypes.INTEGER
  },
  ingrediente_id: {
    type: DataTypes.INTEGER
  },
  quantidade: {
    type: DataTypes.STRING
  },
  medida_id: {
    type: DataTypes.INTEGER
  },
  status: {
    type: DataTypes.INTEGER
  }
}, {
  sequelize,
  modelName: 'itensReceitas'
});


ItensReceita.belongsTo(Receita, { foreignKey: 'receita_id' });
ItensReceita.belongsTo(Ingrediente, { foreignKey: 'ingrediente_id' });
ItensReceita.belongsTo(Medida, { foreignKey: 'medida_id' });

module.exports = ItensReceita;