module.exports = (app) => {

    const controller = require('./controller');

    app.post('/itens', controller.create);

    app.get('/itens/:id', controller.findAll);

    app.put('/itens', controller.update);

    app.delete('/itens', controller.destroy);

    app.post('/insert-itens', controller.insert);
} 