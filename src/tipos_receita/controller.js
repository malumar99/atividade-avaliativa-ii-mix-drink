const db = require('./../configs/sequelize');
const TiposReceita = require('./model');


exports.create = (req,res) =>{
    TiposReceita.create({
        descricao: req.body.descricao,
        status: req.body.status
    }).then((tiposReceita) => {
        res.send(tiposReceita);
    });
}


exports.findAll = (req,res) =>{
    TiposReceita.findAll().then((tiposReceitas) => {
        res.send(tiposReceitas);
    });
}

exports.update = (req, res) => {
    try {
        TiposReceita.update({
            descricao: req.body.descricao,
            status: req.body.status
        }, {
            where: {
                id: req.body.id
            }
        }).then((user) => {
            res.send(user);
        }).catch((err) => {
            res.send(err);
        });
    } catch (error) {
        console.log(error);
    }

}

exports.destroy = (req, res) => {
    try {
        TiposReceita.destroy({
            where: {
                id: req.body.id
            }
        }).then((user) => {
            res.send(true);
        }).catch((err) => {
            res.send(err);
        });
    } catch (error) {
        console.log(error);
    }

}

exports.insert = (req, res) => {
    try {
        db.sequelize.query(`
        insert INTO "tiposReceitas" (id, descricao, status, "createdAt", "updatedAt") VALUES
        (2, 'Alcoólicos',  1, '2021-06-11 23:00:00', '2021-06-11 23:00:00'),
        (3, 'Sem Álcool',  1, '2021-06-11 23:00:00', '2021-06-11 23:00:00'),
        (13, 'Saudáveis', 1, '2021-06-11 23:00:00', '2021-06-11 23:00:00')`).then((tiposReceita) => {
            res.send(tiposReceita);
        }).catch((err) => {
            res.send(err);
        });
    } catch (error) {
        console.log(error);
    }

}