module.exports = (app) => {

    const controller = require('./controller');

    app.post('/tiposReceita', controller.create);

    app.get('/tiposReceita', controller.findAll);

    app.put('/tiposReceita', controller.update);

    app.delete('/tiposReceita', controller.destroy);

    app.post('/insert-tiposReceita', controller.insert);
} 