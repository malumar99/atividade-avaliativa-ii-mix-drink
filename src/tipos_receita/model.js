const db = require('./../configs/sequelize');
const {DataTypes, Model } = db.Sequelize;

const sequelize = db.sequelize;

class TiposReceita extends Model {}


TiposReceita.init({
  descricao: {
    type: DataTypes.STRING
  },
  status: {
    type: DataTypes.INTEGER
  }
}, {
  // Other model options go here
  sequelize, // We need to pass the connection instance
  modelName: 'tiposReceitas' // We need to choose the model name
});

module.exports = TiposReceita;