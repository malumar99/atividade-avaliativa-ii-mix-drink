const db = require('../configs/sequelize');
const {DataTypes, Model } = db.Sequelize;

const sequelize = db.sequelize;

class Ingrediente extends Model {}


Ingrediente.init({
  nome: {
    type: DataTypes.STRING
  },
  status: {
    type: DataTypes.INTEGER
  }
}, {
  sequelize,
  modelName: 'ingredientes'
});

module.exports = Ingrediente;