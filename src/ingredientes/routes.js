module.exports = (app) => {

    const controller = require('./controller');

    app.post('/ingredientes', controller.create);

    app.get('/ingredientes', controller.findAll);

    app.put('/ingredientes', controller.update);

    app.delete('/ingredientes', controller.destroy);

    app.post('/insert-ingredientes', controller.insert);
} 