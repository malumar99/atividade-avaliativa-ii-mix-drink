
Receitas = {

    buscaAll : () => {


        var query = location.search.slice(1);
        var partes = query.split('&');
        var data = {};
        partes.forEach(function (parte) {
            var chaveValor = parte.split('=');
            var chave = chaveValor[0];
            var valor = chaveValor[1];
            data[chave] = valor;
        });
        
        console.log(data); 


        $.ajax({
            type: "GET",
            url: '/receitas-visualizar/'+data.tipo,
            data : {},
            success: (data) => { 
                $('#receitas_lista').empty();
                var tabela = '<table class="table table-striped">'+
                                '<tr>'+
                                    '<th>Imagem</th>'+
                                    '<th>Nome</th>'+
                                    '<th>Tipo</th>'+
                                    '<th>Rendimento</th>'+
                                    '<th>Tempo</th>'+
                                    '<th></th>'+
                                '</tr>';




                for(var receita of data)   { 
                    var tabelaIngredientes = '<br><table style="width:50%">'+
                                                '<tr>'+
                                                    '<th>Ingrediente</th>'+
                                                    '<th>Quantidade</th>'+
                                                    '<th>Medida</th>'+
                                                '</tr>';


                    for(var ingrediente of receita.itens)   {
                        tabelaIngredientes +=
                            '<tr>'+
                                '<td>'+ingrediente.nome+'</td>'+
                                '<td>'+ingrediente.quantidade+'</td>'+
                                '<td>'+ingrediente.descricao+'</td>'+
                            '</tr>';
                    }

                    tabelaIngredientes += '</table>';


                    tabela +=   '<tr>'+
                                    '<td style="vertical-align: middle;"> <img  style=" width: 150px;" src="imagens/logo.jpg"> </td>'+
                                    '<td style="vertical-align: middle;">'+receita.nome+'</td>'+
                                    '<td style="vertical-align: middle;">'+receita.descricao+'</td>'+
                                    '<td style="vertical-align: middle;">'+receita.rendimento+' porções</td>'+
                                    '<td style="vertical-align: middle;">'+receita.tempo+' min</td>'+
                                    '<td style="width:100%">'+
                                        '<table style="width:100%">'+

                                            '<tr>'+
                                                '<th>Modo de preparo</th>'+
                                            '</tr>'+

                                            '<tr>'+
                                                '<td>'+receita.preparo+'</td>'+
                                            '</tr>'+

                                            '<tr>'+
                                                '<td><br></td>'+
                                            '</tr>'+

                                            '<tr>'+
                                                '<th>Ingredientes</th>'+
                                            '</tr>'+

                                            '<tr>'+
                                                '<td>'+tabelaIngredientes+'</td>'+
                                            '</tr>'+

                                        '</table>'+
                                    '</td>'+
                                '</tr>';
                    
                }

                tabela += '</table>';

                $('#receitas_lista').html(tabela);
                console.log(data);
            },
            
            error: () => {
                console.log("Ocorreu um erro");
            },
            dataType: 'json'
        });
    },



    
}



$(document).ready(
    () => {
        Receitas.buscaAll();
    }
)