Admin = {


    salvar : () => {
        //debugger;
        var salvar = $("#receita_id").val();
        if(salvar != null && salvar != '' && salvar != undefined){
            Admin.salvarUpdate(); 
        }else{
            Admin.add();
        }

    },


    limpar : () => {

        $("#nome").val(''); 
        $("#descricao").val('');
        $("#tipo").val('');
        $("#rendimento").val('');
        $("#tempo").val('');
        $("#receita_id").val('');
        $("#preparo").val('');
        
        return false;

    },


    add : () => {

        var t = {};

        t.nome          = $("#nome").val(); 
        t.descricao     = $("#descricao").val();
        t.tipo_id       = $("#tipo_id").val();
        t.rendimento    = $("#rendimento").val();
        t.preparo       = $("#preparo").val();
        t.tempo         = $("#tempo").val();
        
        $.ajax({
            type: 'POST',
            url: '/receitas',
            data : t, 
            dataType: "json",
            success: (data) => { 
                Admin.limpar();
                Admin.buscaAll();
            },
        });
        return false;
    },


    findAll : () => {

        $.ajax({
            type: "GET",
            url: '/post',
            data : {'content':$('#content-search').val()},
            success: (data) => { 
                $('#comments').empty();
                for(var post of data)   { 
                    Posts.template(post);
                }
            },
            
            error: () => {
                //console.log("Ocorreu um erro");
            },
            dataType: 'json'
        });
    },


    buscaAll : () => {

        $.ajax({
            type: "GET",
            url: '/receitas',
            data : {},
            success: (data) => { 
                $('#receitas_lista').empty();
                var tabela = '<table class="table table-striped">'+
                                '<tr>'+
                                    '<th>Ações</th>'+
                                    '<th>Nome</th>'+
                                    '<th>Tipo</th>'+
                                    '<th>Rendimento</th>'+
                                    '<th>Tempo</th>'+
                                '</tr>';




                for(var receita of data)   { 
                    tabela +=   '<tr>'+
                                    '<td>'+
                                        '<a class="btn btn-success btn-sm" onclick="Admin.update('+receita.id+')">Editar</a> '+
                                        '<a class="btn btn-danger btn-sm"  onclick="Admin.remove('+receita.id+')">Remover</a> '+
                                        '<a class="btn btn-primary btn-sm"  onclick="Admin.abrirModal('+receita.id+')">Ingrediente</a> '+
                                        '<input type="hidden" id="receita_id_'+receita.id+'" value="'+receita.id+'" >'+
                                        '<input type="hidden" id="receita_obj_'+receita.id+'" value=\''+JSON.stringify(receita)+'\' >'+
                                    '</td>'+
                                    '<td>'+receita.nome+'</td>'+
                                    '<td>'+receita.descricao+'</td>'+
                                    '<td>'+receita.rendimento+' porções</td>'+
                                    '<td>'+receita.tempo+' min</td>'+
                                '</tr>';
                    
                }

                tabela += '</table>';

                $('#receitas_lista').html(tabela);
                //console.log(data);
            },
            
            error: () => {
                //console.log("Ocorreu um erro");
            },
            dataType: 'json'
        });
    },


    update : (id) => {
        //debugger;
        var obj = $("#receita_obj_"+id).val();
        obj = JSON.parse(obj);
        //console.log('----',obj);

        $("#nome").val(obj.nome); 
        $("#descricao").val(obj.descricao);
        $("#tipo_id").val(obj.tipo_id);
        $("#rendimento").val(obj.rendimento);
        $("#preparo").val(obj.preparo);
        $("#tempo").val(obj.tempo);
        $("#receita_id").val(obj.id);
        

        return false;
    },


    salvarUpdate : () => {

        var t = {};

        t.nome          = $("#nome").val(); 
        t.descricao     = $("#descricao").val();
        t.tipo          = $("#tipo").val();
        t.rendimento    = $("#rendimento").val();
        t.preparo       = $("#preparo").val();
        t.tempo         = $("#tempo").val();
        t.receita_id    = $("#receita_id").val();
        t.id            = $("#receita_id").val();

        $.ajax({
            type: "PUT",
            url: '/receitas',
            data : t,
            success: (data) => { 
                Admin.limpar();
                Admin.buscaAll();
            },
            
            error: () => {
                //console.log("Ocorreu um erro");
            },
            dataType: 'json'
        });

        return false;


    },


    remove : (id) => {

        $.ajax({
            type: "DELETE",
            url: '/receitas',
            data : {'id':id},
            success: (data) => { 
                Admin.buscaAll();
            },
            
            error: () => {
                //console.log("Ocorreu um erro");
            },
            dataType: 'json'
        });

        return false;


    },

    
    abrirModal : (id) => {

        //$('receita_id').val(id);
        $('#receita_item_id').val(id);
        $('#myModal').modal();
        Medida.findAll();
        Ingredientes.findAll();
        Item.buscaAll(id);
        return false;


    }

    
}




Item = {



    limpar : () => {

        $("#ingrediente_id").val(''); 
        $("#quantidade").val(''); 
        $("#medida_id").val(''); 
        
        return false;

    },


    add : () => {

        var t = {};
        //debugger;
        t.ingrediente_id = $("#ingrediente_id").val(); 
        t.quantidade     = $("#quantidade").val();
        t.medida_id      = $("#medida_id").val();
        t.receita_id     = $("#receita_item_id").val();
        
        $.ajax({
            type: 'POST',
            url: '/itens',
            data : t, 
            dataType: "json",
            success: (data) => { 
                Item.limpar();
                Item.buscaAll(t.receita_id);
            },
        });
        return false;
    },


    buscaAll : (id) => {
        //debugger;
        $.ajax({
            type: "GET",
            url: '/itens/'+id,
            data : {},
            success: (data) => { 
                //debugger;
                //$('#receitas_itens').empty();
                var tabela = '<table class="table table-striped">'+
                                '<tr>'+
                                    '<th>Ações</th>'+
                                    '<th>Quantidade</th>'+
                                    '<th>Medida</th>'+
                                    '<th>Ingrediente</th>'+
                                '</tr>';




                for(var receita of data)   { 
                    tabela +=   '<tr>'+
                                    '<td>'+
                                        '<a class="btn btn-danger btn-sm"  onclick="Item.remove('+receita.id+')">Remover</a> '+
                                        '<input type="hidden" id="ingrediente_id_'+receita.id+'" value="'+receita.id+'" >'+
                                    '</td>'+
                                    '<td>'+receita.quantidade+'</td>'+
                                    '<td>'+receita.descricao+'</td>'+
                                    '<td>'+receita.nome+'</td>'+
                                '</tr>';
                    
                }

                tabela += '</table>';

                $('#receitas_itens').html(tabela);
                //console.log(data);
            },
            
            error: () => {
                //console.log("Ocorreu um erro");
            },
            dataType: 'json'
        });
    },


    remove : (id) => {

        var receita = $('#receita_item_id').val();

        $.ajax({
            type: "DELETE",
            url: '/itens',
            data : {'id':id},
            success: (data) => { 
                Item.buscaAll(receita);
            },
            
            error: () => {
                //console.log("Ocorreu um erro");
            },
            dataType: 'json'
        });

        return false;


    }

}


Tipo = {
    findAll : () => {

        $.ajax({
            type: "GET",
            url: '/tiposReceita',
            success: Tipo.loadAll
        }); 
    },

    loadAll: (data) => {
        var tipo = $('#tipo_id');
        //console.log(data, 'aaa');
        for(tipo_receita of data){
            tipo.append($('<option></option>').attr('value', tipo_receita.id).html(tipo_receita.descricao));
        }
    },
   
}


Medida = {
    findAll : () => {

        $.ajax({
            type: "GET",
            url: '/medidas',
            success: Medida.loadAll
        }); 
    },

    loadAll: (data) => {
        //debugger;
        var tipo = $('#medida_id');
        //console.log(data, 'aaa');
        for(ingrediente of data){
            tipo.append($('<option></option>').attr('value', ingrediente.id).html(ingrediente.descricao));
        }
    },
   
}

Ingredientes = {
    findAll : (id) => {

        $.ajax({
            type: "GET",
            url: '/ingredientes',
            success: Ingredientes.loadAll
        }); 
    },

    loadAll: (data) => {
        //debugger;
        var tipo = $('#ingrediente_id');
        //console.log(data, 'aaa');
        for(ingrediente of data){
            tipo.append($('<option></option>').attr('value', ingrediente.id).html(ingrediente.nome));
        }
    },
   
}

$(document).ready(
    () => {
        Tipo.findAll();
        Admin.buscaAll();
    }
)