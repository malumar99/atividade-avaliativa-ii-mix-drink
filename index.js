try {
  
  const express = require('express');
  const app = express();
  const port = 3000;


  const bodyParser = require('body-parser');
  const db = require('./src/configs/sequelize');

  const passport = require('passport');
  const session = require('express-session');


  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({extended:true}));

  
  app.use(express.static("public"));

  // db.sequelize.sync({force:true}).then(()=>{
  //     console.log('teste');
  // }).catch((err) => {
  //     console.log('teste2', err);
  // });


  db.sequelize.sync().then(()=>{
    console.log('teste');
  }).catch((err) => {
      console.log('teste2');
  });



  require('./auth')(passport);

  app.use(session({  
    secret: '123',//configure um segredo seu aqui,
    resave: false,
    saveUninitialized: false,
    cookie: { maxAge: 1 * 60 * 1000 }//30min
  }))
  app.use(passport.initialize());
  app.use(passport.session());



  require('./src/user/routes')(app);
  require('./src/ingredientes/routes')(app);
  require('./src/itens_receita/routes')(app);
  require('./src/medidas/routes')(app);
  require('./src/receitas/routes')(app);
  require('./src/tipos_receita/routes')(app);


  function authenticationMiddleware(req, res, next) {
    if (req.isAuthenticated()) return next();
    res.redirect('/login');
  }




  app.get("/", (req,res) => {
    res.sendFile(__dirname + "/public/views/index.html");
  });


  app.get("/visualizar-receitas", (req,res) => {
    res.sendFile(__dirname + "/public/views/receitas.html");
  });



  app.get("/administrador",authenticationMiddleware, (req,res) => {
    res.sendFile(__dirname + "/public/views/administrador.html");
  });

  // app.get("/login", (req,res) => {
  //   res.sendFile(__dirname + "/public/views/login.html");
  // });



  /* GET login page. */
  app.get('/login', (req, res, next) => {
    if (req.query.fail){
    console.log('Usuário e/ou senha incorretos!',req.query.fail);
        //res.render('login', { message: 'Usuário e/ou senha incorretos!' });
    }else{
    console.log('ok');
        //res.render('login', { message: null });
    }
    res.sendFile(__dirname + "/public/views/login.html");
  });

  /* POST login page */
  app.post('/logar',
    passport.authenticate('local', { 
        successRedirect: '/administrador', 
        failureRedirect: '/login?fail=true' 
    })
  );
  


  app.listen(port, () => {
      console.log(`Example app listening at http://localhost:${port}`);
  });

} catch (error) {
  console.log('erro;', error);
}